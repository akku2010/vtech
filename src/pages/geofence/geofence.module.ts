import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GeofencePage } from './geofence';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    GeofencePage,
  ],
  imports: [
    IonicPageModule.forChild(GeofencePage),
    TranslateModule.forChild()
  ],
})
export class GeofencePageModule {}
